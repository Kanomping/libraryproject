/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Kanomping
 */
public class User {
    int userId;
    String loginName;
    String password;
    String name;
    String surename;
    int typeId;

    public User() {
        this.userId = -1;
    }

    public User(int userId, String loginName, String password, String name, String surename, int typeId) {
        this.userId = userId;
        this.loginName = loginName;
        this.password = password;
        this.name = name;
        this.surename = surename;
        this.typeId = typeId;
    }

    public int getUserId() {
        return userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurename() {
        return surename;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurename(String surename) {
        this.surename = surename;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", loginName=" + loginName + ", password=" + password + ", name=" + name + ", surename=" + surename + ", typeId=" + typeId + '}';
    }
    
    
}
